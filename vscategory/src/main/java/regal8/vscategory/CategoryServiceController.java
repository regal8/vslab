package regal8.vscategory;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import vsshared.SharedCategory;

@RestController
public class CategoryServiceController {
	
	@Autowired
	private CategoryRepository repository;

	@RequestMapping(value = "/categories", method = RequestMethod.POST)
	public ResponseEntity<?> addCategory(@RequestBody SharedCategory category) {
		String categoryName = category.getCategoryName();
		
		//check if the necessary attributes are set
		if (categoryName == null  || categoryName.isEmpty()) {
			return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
		}
		
		//check if a category of this name already exists
		long nameMatches = StreamSupport.stream(repository.findAll().spliterator(), false)
		.filter(x -> x.getName().equals(categoryName)).count();
		
		if (nameMatches > 0) {
			return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
		}

		//create category
		repository.save(new Category(category));
		return new ResponseEntity<>(null, HttpStatus.CREATED);
	}
	
	@RequestMapping(value = "/categories", method = RequestMethod.GET)
	public ResponseEntity<Iterable<SharedCategory>> getCategories(@RequestParam(name = "name", required = false) String categoryName) {
		
		List<SharedCategory> sharedCategories = new ArrayList<>();
		
		if (categoryName == null || categoryName.isEmpty()) {
			// retrieve all categories
			Iterable<Category> categories = repository.findAll();
			for (Category category: categories) {
				sharedCategories.add(category.exportToSharedCategory());
			}
			return new ResponseEntity<>(sharedCategories, HttpStatus.OK);
		}
		else {
			// retrieve this category
			List<Category> categoryList = StreamSupport.stream(repository.findAll().spliterator(), false)
					.filter(x -> x.getName().equals(categoryName)).collect(Collectors.toList()); 
					
			if (categoryList.size() == 1) {
				sharedCategories.add(categoryList.get(0).exportToSharedCategory());
				return new ResponseEntity<>(sharedCategories, HttpStatus.OK);
			}
			
			// if we reach this point, no matching category has been found
			return new ResponseEntity<>(sharedCategories, HttpStatus.NOT_FOUND);
		}
	}
	
	@RequestMapping(value = "/categories/{id}", method = RequestMethod.GET)
	public ResponseEntity<SharedCategory> getCategoryById(@PathVariable Long id) {
		Optional<Category> category = repository.findById(id);
		if (category.isPresent()) {
			return new ResponseEntity<>(category.get().exportToSharedCategory(), HttpStatus.OK);
		}
		else {
			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		}
	}
	
	@RequestMapping(value = "/categories/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<?> deleteCategoryById(@PathVariable Long id) {
		Optional<Category> category = repository.findById(id);
		if (category.isPresent()) {
			repository.delete(category.get());
			return new ResponseEntity<>(null, HttpStatus.OK);
		}
		else {
			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		}
	}
	
}