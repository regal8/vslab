package regal8.vscategory;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import vsshared.SharedCategory;

@Entity
@Table(name="category")
public class Category {
	
	@Id
	@GeneratedValue
	@Column(name="id")
	private Long id;
	
	@Column(name="name", unique=true)
	private String name;

	public Category() {
		
	}
	
	public Category(SharedCategory category) {
		setId(category.getId());
		setName(category.getCategoryName());
	}
	
	public SharedCategory exportToSharedCategory() {
		return new SharedCategory(id, name);
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
}
	
