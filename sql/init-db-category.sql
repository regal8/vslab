CREATE DATABASE IF NOT EXISTS db_category;

grant all on db_category.* to 'webshopuser'@'%';

USE db_category;

CREATE TABLE category (
	id INT NOT NULL AUTO_INCREMENT,
	name VARCHAR(255) NOT NULL,
	PRIMARY KEY (id)
) ENGINE=InnoDB;


