#!/bin/sh

## comment in as desired, to rebuild/restart

## Building microservices and docker images
#cd vsshared && mvn clean install && cd ..

## Building microservices and docker images
#docker build -t vs-categoryservice ./vscategory
#docker build -t vs-productservice ./vsproduct
#docker build -t vs-catalogservice ./vscatalog

## Building initialized MySQL Database image
#docker build -t vs-db-image -f ./docker/DockerfileMySQL .

## Stopping and removing old containers
docker-compose stop && docker-compose rm -f

## Composing microservice containers
docker-compose up -d --remove-orphans && docker-compose logs -tf

