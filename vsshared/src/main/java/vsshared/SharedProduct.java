package vsshared;

public class SharedProduct {
	private Long id;
	private String productName;
	private Long categoryId;
	private String categoryName;
	private Float price;
	private String details;
	
	public SharedProduct() {
		
	}
	
	public SharedProduct(Long id, String productName, Long categoryId, Float price, String details) {
		this.id = id;
		this.productName = productName;
		this.categoryId = categoryId;
		this.price = price;
		this.details = details;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public Long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}

	public Float getPrice() {
		return price;
	}

	public void setPrice(Float price) {
		this.price = price;
	}

	public String getDetails() {
		return details;
	}

	public void setDetails(String details) {
		this.details = details;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
}
