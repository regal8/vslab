package vsshared;

public class SharedCategory {

	private Long id;
	private String categoryName;

	public SharedCategory() {
		
	}
	
	public SharedCategory(Long id, String categoryName) {
		this.id = id;
		this.categoryName = categoryName;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	
	
}
