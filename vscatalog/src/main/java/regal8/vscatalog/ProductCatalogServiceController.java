package regal8.vscatalog;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import vsshared.SharedCategory;
import vsshared.SharedProduct;


@RestController
public class ProductCatalogServiceController {
	
	private static final String CATEGORY_URI="http://categoryservice:8080/categories";
	private static final String PRODUCT_URI="http://productservice:8080/products";
	
	@Autowired
	private RestTemplate restTemplate;
	
	@RequestMapping(value = "/categories", method = RequestMethod.POST)
	public ResponseEntity<?> addCategory(@RequestBody SharedCategory category) {
		ResponseEntity<?> response;
		try {
			response = restTemplate.postForEntity(CATEGORY_URI, category, null);
		}
		catch (HttpClientErrorException e) {
			if (e.getStatusCode().is4xxClientError()) {
				return new ResponseEntity<>(null, e.getStatusCode());
			}
			else {
				throw new HttpClientErrorException(e.getStatusCode());
			}
		}
		return new ResponseEntity<>(null, response.getStatusCode());
	}
	
	@RequestMapping(value = "/categories", method = RequestMethod.GET)
	public ResponseEntity<Iterable<SharedCategory>> getCategories(@RequestParam(name = "name", required = false) String categoryName) {
		ResponseEntity<Iterable<SharedCategory>> response;
		if (categoryName != null && !categoryName.isEmpty()) {
			try {
				response = restTemplate.exchange(CATEGORY_URI + "?name=" + categoryName, HttpMethod.GET, null, new ParameterizedTypeReference<Iterable<SharedCategory>>() {});
			}
			catch (HttpClientErrorException e) {
				if (e.getStatusCode().is4xxClientError()) {
					return new ResponseEntity<>(null, e.getStatusCode());
				}
				else {
					throw new HttpClientErrorException(e.getStatusCode());
				}
			}
		}
		else {
			try {
				response = restTemplate.exchange(CATEGORY_URI, HttpMethod.GET, null, new ParameterizedTypeReference<Iterable<SharedCategory>>() {});
			}
			catch (HttpClientErrorException e) {
				if (e.getStatusCode().is4xxClientError()) {
					return new ResponseEntity<>(null, e.getStatusCode());
				}
				else {
					throw new HttpClientErrorException(e.getStatusCode());
				}
			}
		}
		return new ResponseEntity<>(response.getBody(), response.getStatusCode());
	}
	
	@RequestMapping(value = "/categories/{id}", method = RequestMethod.GET)
	public ResponseEntity<SharedCategory> getCategoryById(@PathVariable Long id) {
		ResponseEntity<SharedCategory> response;
		try {
			response = restTemplate.getForEntity(CATEGORY_URI + "/{id}", SharedCategory.class, id);
		}
		catch (HttpClientErrorException e) {
			if (e.getStatusCode().is4xxClientError()) {
				return new ResponseEntity<>(null, e.getStatusCode());
			}
			else {
				throw new HttpClientErrorException(e.getStatusCode());
			}
		}
		return new ResponseEntity<>(response.getBody(), response.getStatusCode());
	}
	
	@RequestMapping(value = "/categories/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<?> deleteCategoryById(@PathVariable Long id) {
		
		ResponseEntity<SharedCategory> categoryResponse = getCategoryById(id);
		
		if (categoryResponse.getStatusCode() == HttpStatus.NOT_FOUND) {
			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		}
		else {
			// consistent deletion: first all the products of that category,
			//  then the category itself
			restTemplate.delete(PRODUCT_URI + "?categoryId=" + id.toString());
			restTemplate.delete(CATEGORY_URI + "/{id}", id);
			return new ResponseEntity<>(null, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/products", method = RequestMethod.POST)
	public ResponseEntity<?> addProduct(@RequestBody SharedProduct product) {
		
		// check if category exists
		Long categoryId = product.getCategoryId();
		ResponseEntity<SharedCategory> categoryResponse = getCategoryById(categoryId);
		if (!categoryResponse.getStatusCode().is2xxSuccessful()) {
			return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
		}
		else {
			ResponseEntity<?> response = restTemplate.postForEntity(PRODUCT_URI, product, null);
			return new ResponseEntity<>(null, response.getStatusCode());
		}
	}
	
	@RequestMapping(value = "/products", method = RequestMethod.GET)
	public ResponseEntity<Iterable<SharedProduct>> getProducts(@RequestParam(name = "minPrice", required = false) Float minPrice,
			@RequestParam(name = "maxPrice", required = false) Float maxPrice,
			@RequestParam(name = "detailsSearch", required = false) String detailsSearch) {
		
		StringBuilder queryBuilder = new StringBuilder();
		boolean atleastOneParameter = false;
		if (minPrice != null) {
			queryBuilder.append("?minPrice=").append(minPrice.toString());
			atleastOneParameter = true;
		}
		if (maxPrice != null) {
			queryBuilder.append(atleastOneParameter ? '&' : '?')
				.append("maxPrice=").append(maxPrice.toString());
			atleastOneParameter = true;
		}
		if (detailsSearch != null && !detailsSearch.isEmpty()) {
			queryBuilder.append(atleastOneParameter ? '&' : '?')
				.append("detailsSearch=").append(detailsSearch);
		}
		ResponseEntity<Iterable<SharedProduct>> response = restTemplate.exchange(PRODUCT_URI + queryBuilder.toString(), HttpMethod.GET, null, new ParameterizedTypeReference<Iterable<SharedProduct>>() {});
		if (response.getStatusCode().is2xxSuccessful()) {
			for (SharedProduct product: response.getBody()) {
				// fill in the category name for each product
				SharedCategory category = getCategoryById(product.getCategoryId()).getBody();
				if (category == null) {
					// error: inconsistent data
					return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
				}
				else {
					product.setCategoryName(category.getCategoryName());
				}
			}
		}
		return new ResponseEntity<>(response.getBody(), response.getStatusCode());
	}
	
	@RequestMapping(value = "/products/{id}", method = RequestMethod.GET)
	public ResponseEntity<SharedProduct> getProductById(@PathVariable Long id) {
		ResponseEntity<SharedProduct> response;
		try {
			response = restTemplate.getForEntity(PRODUCT_URI + "/{id}", SharedProduct.class, id);
		}
		catch (HttpClientErrorException e) {
			if (e.getStatusCode().is4xxClientError()) {
				return new ResponseEntity<>(null, e.getStatusCode());
			}
			else {
				throw new HttpClientErrorException(e.getStatusCode());
			}
		}
		if (response.getStatusCode().is2xxSuccessful()) {
			SharedProduct product= response.getBody();
			SharedCategory category = getCategoryById(product.getCategoryId()).getBody();
			if (category == null) {
				// error: inconsistent data, product references a non-existing category
				return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
			}
			else {
				product.setCategoryName(category.getCategoryName());
			}
		}
		return new ResponseEntity<>(response.getBody(), response.getStatusCode());
	}
	
	@RequestMapping(value = "/products/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<?> deleteProductById(@PathVariable Long id) {
		ResponseEntity<SharedProduct> productResponse = getProductById(id);
		if (productResponse.getStatusCode() == HttpStatus.NOT_FOUND) {
			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		}
		else {
			restTemplate.delete(PRODUCT_URI + "/{id}", id);
			return new ResponseEntity<>(null, HttpStatus.OK);
		}
	}


	}
