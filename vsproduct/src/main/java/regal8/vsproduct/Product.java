package regal8.vsproduct;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import vsshared.SharedProduct;

@Entity
@Table(name="product")
public class Product {
	
	@Id
	@GeneratedValue
	@Column(name="id")
	private Long id;
	
	@Column(name="name")
	private String name;
	
	@Column(name="category_id")
	private Long category_id;
	
	@Column(name="price")
	private Float price;
	
	@Column(name="details")
	private String details;

	public Product() {
		
	}
	
	public Product(SharedProduct product) {
		setId(product.getId());
		setCategory_id(product.getCategoryId());
		setName(product.getProductName());
		setPrice(product.getPrice());
		setDetails(product.getDetails());
	}
	
	public SharedProduct exportToSharedProduct() {
		return new SharedProduct(id, name, category_id, price, details);
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getCategory_id() {
		return category_id;
	}

	public void setCategory_id(Long categoryId) {
		this.category_id = categoryId;
	}

	public Float getPrice() {
		return price;
	}

	public void setPrice(Float price) {
		this.price = price;
	}

	public String getDetails() {
		return details;
	}

	public void setDetails(String details) {
		this.details = details;
	}
	
}
