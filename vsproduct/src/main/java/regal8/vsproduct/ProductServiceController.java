package regal8.vsproduct;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import vsshared.SharedProduct;

@RestController
public class ProductServiceController {
	
	@Autowired
	private ProductRepository repository;

	@RequestMapping(value = "/products", method = RequestMethod.POST)
	public ResponseEntity<?> addProduct(@RequestBody SharedProduct product) {
		String productName = product.getProductName();
		Long categoryID = product.getCategoryId();
		
		//check if the necessary attributes are set
		if (productName == null  || productName.isEmpty() || categoryID == null) {
			return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
		}
		
		//create product
		repository.save(new Product(product));
		return new ResponseEntity<>(null, HttpStatus.CREATED);
	}
	
	@RequestMapping(value = "/products", method = RequestMethod.GET)
	public ResponseEntity<Iterable<SharedProduct>> getProducts(@RequestParam(name = "minPrice", required = false) Float minPrice,
			@RequestParam(name = "maxPrice", required = false) Float maxPrice,
			@RequestParam(name = "detailsSearch", required = false) String detailsSearch) {
		
		if (minPrice == null) {
			minPrice = 0.0f;
		}
		
		if (maxPrice == null) {
			maxPrice = Float.MAX_VALUE;
		}
		
		final float queryMinPrice = minPrice;
		final float queryMaxPrice = maxPrice;
		
		
		List<Product> products;
		
		if (detailsSearch == null || detailsSearch.isEmpty()) {
			products = StreamSupport.stream(repository.findAll().spliterator(), false)
					.filter(x -> (x.getPrice() >= queryMinPrice && x.getPrice() <= queryMaxPrice))
					.collect(Collectors.toList());  
		}
		else {
			products = StreamSupport.stream(repository.findAll().spliterator(), false)
					.filter(x -> (x.getPrice() >= queryMinPrice && x.getPrice() <= queryMaxPrice
					&& x.getDetails().toLowerCase().contains(detailsSearch.toLowerCase())))
					.collect(Collectors.toList());
		}
		
		List<SharedProduct> sharedProducts = new ArrayList<>();
		for (Product product: products) {
			sharedProducts.add(product.exportToSharedProduct());
		}
		
		return new ResponseEntity<>(sharedProducts, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/products", method = RequestMethod.DELETE)
	public ResponseEntity<?> deleteProductsByCategoryId(@RequestParam(name="categoryId") Long categoryId) {
		List<Product> products = StreamSupport.stream(repository.findAll().spliterator(), false)
				.filter(x -> x.getCategory_id() == categoryId).collect(Collectors.toList()); 
				
		repository.deleteAll(products);
		
		return new ResponseEntity<>(null, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/products/{id}", method = RequestMethod.GET)
	public ResponseEntity<SharedProduct> getProductById(@PathVariable Long id) {
		Optional<Product> product = repository.findById(id);
		if (product.isPresent()) {
			return new ResponseEntity<>(product.get().exportToSharedProduct(), HttpStatus.OK);
		}
		else {
			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		}
	}
	
	@RequestMapping(value = "/products/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<?> deleteProductById(@PathVariable Long id) {
		Optional<Product> product = repository.findById(id);
		if (product.isPresent()) {
			repository.delete(product.get());
			return new ResponseEntity<>(null, HttpStatus.OK);
		}
		else {
			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		}
	}
}
